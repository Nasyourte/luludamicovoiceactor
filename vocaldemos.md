---
layout: vocaldemos.html
title1: Singing
title2: Voice Acting

websiteName:
    Lulu D'Amico - Voice Actor
home: 
    Home 
vocal: 
    Vocal Demo's
contact:
    Contact
pageName:
    Vocal demo's
titleContact:
    Contact
mailText:
    Email
contactMail:
    Ocimadalia@Gmail.com
textMandyProfile:
    Mandy Profile
mandyProfile:
    https://www.mandy.com/uk/v/lulu-damico
ukPhone1:
    +447843824367
ukPhone2:
    +44 7843824367
ukPhoneText:
    UK Phone
frPhone1:
    +33638218791
frPhone2:
    +33 638218791
frPhoneText:
    French Phone



vocals:
    -
        image_vocals: ../ressources/images/1.jpg
        vocalsText: Lorem ipsum dolor sit amet consectetur adipisicing elit Ea quidem aperiam
        vocalsSound: ../ressources/sound/testSound.mp3

voiceActing:
    -
        image_voiceActing: ../ressources/images/1.jpg
        voiceActingText: Lorem ipsum dolor sit amet consectetur adipisicing elit Ea quidem aperiam
        voiceActingSound: ../ressources/sound/testSound.mp3
---